import codescanner.CodeScanner;
import codescanner.CodeScannerReport;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        List<String> inputData = main.loadInputData();
        CodeScanner syntaxScanner = new CodeScanner(inputData);

        CodeScannerReport codeScannerReport = syntaxScanner.scanCodeLines();
        System.out.printf("Out of %d code lines, %d syntax errors were found, which gave a total score of %d points.\n" + "%d incomplete code lines were found, and the middle score was %d points.%n", codeScannerReport.getNumberOfCodeLines(), codeScannerReport.getNumberOfSyntaxErrors(), codeScannerReport.getSyntaxErrorPoints(), codeScannerReport.getNumberOfIncompleteLines(), codeScannerReport.getMiddleScore());

    }

    private List<String> loadInputData() {
        List<String> inputData = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File("src/main/resources/input_data.txt"))) {
            while (scanner.hasNextLine()) {
                inputData.add(scanner.nextLine().trim());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(0);
        }

        return inputData;
    }
}
