package codescanner;

import java.math.BigInteger;

public class CodeScannerReport {
    private int numberOfCodeLines;
    private int numberOfSyntaxErrors;
    private BigInteger syntaxErrorPoints;
    private int numberOfIncompleteLines;
    private BigInteger middleScore;

    public int getNumberOfCodeLines() {
        return numberOfCodeLines;
    }

    public void setNumberOfCodeLines(int numberOfCodeLines) {
        this.numberOfCodeLines = numberOfCodeLines;
    }

    public int getNumberOfSyntaxErrors() {
        return numberOfSyntaxErrors;
    }

    public void setNumberOfSyntaxErrors(int numberOfSyntaxErrors) {
        this.numberOfSyntaxErrors = numberOfSyntaxErrors;
    }

    public BigInteger getSyntaxErrorPoints() {
        return syntaxErrorPoints;
    }

    public void setSyntaxErrorPoints(BigInteger syntaxErrorPoints) {
        this.syntaxErrorPoints = syntaxErrorPoints;
    }

    public int getNumberOfIncompleteLines() {
        return numberOfIncompleteLines;
    }

    public void setNumberOfIncompleteLines(int numberOfIncompleteLines) {
        this.numberOfIncompleteLines = numberOfIncompleteLines;
    }

    public BigInteger getMiddleScore() {
        return middleScore;
    }

    public void setMiddleScore(BigInteger middleScore) {
        this.middleScore = middleScore;
    }
}
