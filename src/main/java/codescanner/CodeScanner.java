package codescanner;

import java.math.BigInteger;
import java.util.*;

public class CodeScanner {
    private final List<String> codeLines;

    private static final Map<String, String> CHARACTERS = new HashMap<>() {{
        put("(", ")");
        put("[", "]");
        put("{", "}");
        put("<", ">");
    }};

    private static final Map<String, BigInteger> CHARACTERS_SYNTAX_ERROR_POINT_MAP = new HashMap<>() {{
        put(")", BigInteger.valueOf(3));
        put("]", BigInteger.valueOf(57));
        put("}", BigInteger.valueOf(1197));
        put(">", BigInteger.valueOf(25137));
    }};

    private static final Map<String, BigInteger> CHARACTERS_ADDITIONAL_CHARS_POINT_MAP = new HashMap<>() {{
        put(")", BigInteger.valueOf(1));
        put("]", BigInteger.valueOf(2));
        put("}", BigInteger.valueOf(3));
        put(">", BigInteger.valueOf(4));
    }};

    public CodeScanner(List<String> codeLines) {
        this.codeLines = codeLines;
    }

    public CodeScannerReport scanCodeLines() {
        final List<String> syntaxErrors = new ArrayList<>();
        final List<BigInteger> totalAddCharactersPoints = new ArrayList<>();

        // Iterate over every line of code
        for (String codeLine : codeLines) {

            // First, remove all pairs, like () and {} from line
            codeLine = removePairs(codeLine);

            // Check if line is not complete, then complete it and calculate point and then go back to next code line
            if (isIncomplete(codeLine)) {
                completeIncorrectLineAndCalculatePoints(codeLine, totalAddCharactersPoints);
                continue;
            }

            // Search for first syntax error
            identifyPossibleSyntaxError(codeLine, syntaxErrors);
        }

        // Calculate all scanning data and return report
        CodeScannerReport codeScannerReport = new CodeScannerReport();
        codeScannerReport.setNumberOfCodeLines(codeLines.size());
        codeScannerReport.setMiddleScore(calculateMiddlePoint(totalAddCharactersPoints));
        codeScannerReport.setNumberOfIncompleteLines(totalAddCharactersPoints.size());
        codeScannerReport.setNumberOfSyntaxErrors(syntaxErrors.size());
        codeScannerReport.setSyntaxErrorPoints(calculateSyntaxPoints(syntaxErrors));

        return codeScannerReport;
    }

    private void identifyPossibleSyntaxError(String codeLine, List<String> syntaxErrors) {
        // Now we must search for the first incorrect end character, if any
        for (int i = 0; i < codeLine.length(); i++) {

            String currentCharacter = String.valueOf(codeLine.charAt(i));

            // Iterate over the entry set of character
            if (CHARACTERS.containsValue(currentCharacter)) {
                syntaxErrors.add(currentCharacter);
                return;
            }
        }
    }

    private boolean isIncomplete(String codeLine) {

        // Check if line contain any end chars, if so, then line is incomplete
        boolean onlyEndChars = true;

        for (int i = 0; i < codeLine.length(); i++) {

            String currentCharacter = String.valueOf(codeLine.charAt(i));

            // Iterate over the entry set of character
            if (CHARACTERS.containsValue(currentCharacter)) {
                onlyEndChars = false;
                break;
            }
        }

        return onlyEndChars;
    }

    private String removePairs(String codeLine) {
        boolean continueScan = true;

        // Iterate over a line of code and identify pairs.
        while (continueScan) {
            boolean pairFound = false;
            final int codeLineLength = codeLine.length();
            for (int i = 0; i < codeLineLength; i++) {

                // Avoid exception on last character
                if (i != codeLineLength - 1) {

                    // Get current and next character from code line
                    String currentCharacter = String.valueOf(codeLine.charAt(i));
                    String nextCharacter = String.valueOf(codeLine.charAt(i + 1));

                    // Get the end identifier for the current character
                    String chunkEndIdentifier = CHARACTERS.get(currentCharacter);

                    // If the next character is equal to the end identifier, then remove them from code line
                    // and start the iteration of the code line again
                    if (nextCharacter.equals(chunkEndIdentifier)) {
                        codeLine = codeLine.substring(0, i) + codeLine.substring(i + 2);
                        pairFound = true;
                        break;
                    }
                }
            }

            // No new pairs were found, then stop iterating over the code line
            if (!pairFound) {
                continueScan = false;
            }
        }

        return codeLine;
    }

    private BigInteger calculateMiddlePoint(List<BigInteger> totalAddCharactersPoints) {
        Collections.sort(totalAddCharactersPoints);
        return totalAddCharactersPoints.get(totalAddCharactersPoints.size() / 2);
    }

    private BigInteger calculateSyntaxPoints(List<String> syntaxErrors) {
        BigInteger totalSyntaxPoints = BigInteger.valueOf(0);
        for (String syntaxError : syntaxErrors) {
            totalSyntaxPoints = totalSyntaxPoints.add((CHARACTERS_SYNTAX_ERROR_POINT_MAP.get(syntaxError)));
        }

        return totalSyntaxPoints;
    }


    private void completeIncorrectLineAndCalculatePoints(String codeLine, List<BigInteger> totalAddCharactersPoints) {
        BigInteger points = BigInteger.valueOf(0);

        for (int i = codeLine.length() - 1; i >= 0; i--) {
            String currentCharacter = String.valueOf(codeLine.charAt(i));
            String endCharacter = CHARACTERS.get(currentCharacter);
            points = points.multiply(BigInteger.valueOf(5)).add((CHARACTERS_ADDITIONAL_CHARS_POINT_MAP.get(endCharacter)));
        }

        totalAddCharactersPoints.add(points);
    }
}
